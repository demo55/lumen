<?php


namespace App\Http\Controllers;


use App\Components\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|email|unique:users',
            'full_name' => 'required',
            'password'  => 'required|confirmed|min:6',
        ]);

        $user = $this->userService->create($request->all());

        return response()->json($user);
    }
}
