<?php


namespace App\Components;


use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * create User
     *
     * @param $params
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function create($params)
    {
        Arr::forget($params, ['password_confirmation']);

        $params['password'] = Hash::make('password');;

        return User::query()->create($params);
    }
}
